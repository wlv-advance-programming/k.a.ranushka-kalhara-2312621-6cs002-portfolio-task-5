package task;

import java.util.*;

public class Movie02 {
  public static void main(String[] args) {
	   List<Movie> table = Arrays.asList(
    		new Movie(1, "The Shawshank Redemption", 1994, "Frank Darabont", "Tim Robbins", "Morgan Freeman", "Drama", 142, 25000000, 283500000, 9.3, 80, 90, 7),
    		new Movie(2, "The Godfather", 1972, "Francis Ford Coppola", "Marlon Brando", "Diane Keaton", "Crime", 175, 6000000, 245000000, 9.2, 100, 95, 3),
    		new Movie(3, "The Dark Knight", 2008, "Christopher Nolan", "Christian Bale", "Heath Ledger", "Action", 152, 185000000, 1048430000, 9.0, 84, 94, 2),
    		new Movie(4, "The Godfather: Part II", 1974, "Francis Ford Coppola", "Al Pacino", "Robert De Niro", "Crime", 202, 13000000, 139200000, 9.0, 90, 92, 6),
    		new Movie(5, "Pulp Fiction", 1994, "Quentin Tarantino", "John Travolta", "Uma Thurman", "Crime", 154, 8000000, 107800000, 8.9, 94, 90, 5),
    		new Movie(6, "The Lord of the Rings: The Return of the King", 2003, "Peter Jackson", "Elijah Wood", "Liv Tyler", "Fantasy", 201, 94000000, 1192800000, 9.0, 87, 96, 11),
    		new Movie(7, "12 Angry Men", 1957, "Sidney Lumet", "Henry Fonda", "Lee J. Cobb", "Drama", 96, 350000, 3600000, 8.9, 98, 98, 3)
	    );
    table.stream().forEach(x -> System.out.println(x));
    System.out.println();
    table.parallelStream().forEach(System.out::println);
  }
}