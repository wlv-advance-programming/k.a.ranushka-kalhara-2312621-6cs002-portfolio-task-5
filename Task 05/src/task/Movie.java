package task;

public class Movie implements Comparable<Movie> {
     private int id;
     private String title;
     private int releaseYear;
     private String director;
     private String leadActor;
     private String leadActress;
     private String genre;
     private int runtime;
     private int budget;
     private int boxOfficeRevenue;
     private double imdbRating;
     private int metacriticScore;
     private int rottentomatoesScore;
     private int numberOfAwardsWon;

     public Movie(int id, String title, int releaseYear, String director, String leadActor,
                  String leadActress, String genre, int runtime, int budget, int boxOfficeRevenue,
                  double imdbRating, int metacriticScore, int rottentomatoesScore, int numberOfAwardsWon) {
         this.id = id;
         this.title = title;
         this.releaseYear = releaseYear;
         this.director = director;
         this.leadActor = leadActor;
         this.leadActress = leadActress;
         this.genre = genre;
         this.runtime = runtime;
         this.budget = budget;
         this.boxOfficeRevenue = boxOfficeRevenue;
         this.imdbRating = imdbRating;
         this.metacriticScore = metacriticScore;
         this.rottentomatoesScore = rottentomatoesScore;
         this.numberOfAwardsWon = numberOfAwardsWon;
     }

     public int getId() {
         return id;
     }

     public String getTitle() {
         return title;
     }

     public int getReleaseYear() {
         return releaseYear;
     }

     public String getDirector() {
         return director;
     }

     public String getLeadActor() {
         return leadActor;
     }

     public String getLeadActress() {
         return leadActress;
     }

     public String getGenre() {
         return genre;
     }

     public int getRuntime() {
         return runtime;
     }

     public int getBudget() {
         return budget;
     }

     public int getBoxOfficeRevenue() {
         return boxOfficeRevenue;
     }

     public double getImdbRating() {
         return imdbRating;
     }

     public int getMetacriticScore() {
         return metacriticScore;
     }

     public int getRottentomatoesScore() {
         return rottentomatoesScore;
     }

     public int getNumberOfAwardsWon() {
         return numberOfAwardsWon;
     }

     @Override
     public int compareTo(Movie movie) {
         return Integer.compare(this.numberOfAwardsWon, movie.numberOfAwardsWon);
     }

     public String toString() {
    	 return String.format("%-3d%-20s%10d%.1f%10s%10d%10d", id, title, releaseYear, imdbRating, director, budget, boxOfficeRevenue);
     }
  }
